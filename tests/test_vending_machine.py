from enum import Enum

import pytest

# https://avocadoughtoast.com/weights-sizes-us-coins/
# weight, diameter, thickness
NICKEL_MEASURES = (5,.835,1.95)
DIME_MEASURES = (2.268, .705, 1.35)
QUARTER_MEASURES = (5.67, .705, 1.75)

COKE = 'COKE'
DUMMY_PRODUCT = 'DUMMY'

class Coin(Enum):
    NICKEL = 'NICKEL'
    DIME = 'DIME'
    QUARTER = 'QUARTER'

class TestVendingDepositCoins:

    def test_new_vending_machine_balance(self):
        machine = VendingMachine(400)
        assert machine.balance == 400
        assert machine.coin_slot_total == 0

    def test_nickel(self):
        machine = VendingMachine(400)
        machine.deposit_in_coin_slot(*NICKEL_MEASURES)
        assert machine.balance == 400.05
        assert machine.coin_slot_total == 0.05

    def test_dime(self):
        machine = VendingMachine(400)
        machine.deposit_in_coin_slot(*DIME_MEASURES)
        assert machine.balance == 400.10
        assert machine.coin_slot_total == 0.10

    def test_quarter(self):
        machine = VendingMachine(400)
        machine.deposit_in_coin_slot(*QUARTER_MEASURES)
        assert machine.balance == 400.25
        assert machine.coin_slot_total == 0.25

    def test_deposit_multiple_coins(self): 
        machine = VendingMachine(400)
        machine.deposit_in_coin_slot(*QUARTER_MEASURES)
        machine.deposit_in_coin_slot(*DIME_MEASURES)
        machine.deposit_in_coin_slot(*NICKEL_MEASURES)
        assert machine.balance == 400.40
        assert machine.coin_slot_total == 0.25

class TestProduct:
    def test_add_product_price(self):
        machine = VendingMachine(400)
        machine.set_product_price(COKE, 1.25);
        assert machine.get_product_price(COKE) == 1.25;

    def test_when_select_product_then_verify_money_needed(self):
        machine = VendingMachine(400)
        machine.set_product_price(COKE, 1.25)
        machine.select_product(COKE)
        assert machine.money_needed_from_customer == 1.25

    def test_when_multiple_products_and_product_selected_then_verify_money_needed(self):
        raise NotImplementedError


class TestChange:
    def test_add_available_coins(self):
        machine = VendingMachine(400)
        machine.add_available_coins(Coin.QUARTER, 1)
        machine.add_available_coins(Coin.DIME, 3)
        machine.add_available_coins(Coin.NICKEL, 2)
        assert machine.available_coins.includes((Coin.QUARTER, 1))
        assert machine.available_coins.includes((Coin.DIME, 3))
        assert machine.available_coins.includes((Coin.NICKEL, 2))
        assert machine.available_coins.size() == 3
    
    def test_dispense_coins(self):
        machine = VendingMachine(400)
        machine.add_available_coins(Coin.QUARTER, 1)
        machine.add_available_coins(Coin.DIME, 3)
        machine.add_available_coins(Coin.NICKEL, 2)
        machine.add_product(DUMMY_PRODUCT, 30)
        machine.select_product(DUMMY_PRODUCT)
        machine.deposit_in_coin_slot(NICKEL_MEASURES)
        assert machine.balance == 400.05
        machine.dispense_change()
        assert machine.dispensed_coins.includes((Coin.QUARTER, 1))
        assert machine.dispensed_coins.size() == 1






