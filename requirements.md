Provide functionality of a vending machine, including collecting money, 
determining value of a coin by dimensions, dispense products, and dispense change.

Vending Machine
  - Create a vending machine including defining starting balance
  - Actions: Deposit Coin, Select Product, Pay by Credit Card, Insert Dollar
  - Display different messages depending on state
    - READY if no money received from customer
    - $(value) if money received from customer
    - OUT OF (product)
    - NO CHANGE if cannot provide exact amount of change

Coin
  - calculate value of coin based on weight, thickness, and size
  - add coin value to machine balance and money deposited by customer